from django.contrib import admin
from Tarea1.models import Editor, Autor, Libro

admin.site.register(Editor)
admin.site.register(Autor)
admin.site.register(Libro)
