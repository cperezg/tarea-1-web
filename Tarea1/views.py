from django.shortcuts import render
from django.http import HttpResponse
from Tarea1.models import Libro


def index(request):
    return render(request, 'index.html')

def formulario_buscar(request):
    return render(request, 'formulario_buscar.html')

def buscar(request):
    if 'q' in request.GET and request.GET['q']:
        try:
            libro = Libro.objects.get(titulo=request.GET['q'])
        except Libro.DoesNotExist:
            mensaje = 'El libro %r no existe en los registros.' % request.GET['q']
        else:
            autor = libro.autores.get()
            mensaje = '%s - %s %s' % (libro.titulo, autor.nombre, autor.apellidos, )
    else:
        mensaje = 'Ingresa un libro a buscar.'

    return HttpResponse(mensaje)
